import axios from 'axios'

const api = axios.create({
  // baseURL: 'https://redir.dazvid.net', // prod
  baseURL: 'http://localhost:3000/data', // dev
  headers: {
    common: {
      Accept: 'text/plain, */*'
    }
  }
})

export default {
  getAll() {
    return api.get('/')
  },
  create(redirect) {
    return api.post('/', redirect)
  },
  update(redirect) {
    return api.put('/' + redirect.id, redirect)
  },
  delete(id) {
    return api.delete('/' + id)
  }
}
