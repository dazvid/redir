import { NotificationProgrammatic as Notification } from 'buefy'
import RedirectService from '@/services/RedirectService.js'

export const state = () => ({
  redirects: []
})

export const mutations = {
  SET_REDIRECTS(state, redirects) {
    state.redirects = redirects
  },
  ADD_REDIRECT(state, redirect) {
    state.redirects.push(redirect)
  },
  EDIT_REDIRECT(state, updatedRedirect) {
    const index = state.redirects.findIndex(
      (redirect) => redirect.id === updatedRedirect.id
    )
    state.redirects[index].shortcut = updatedRedirect.shortcut
    state.redirects[index].url = updatedRedirect.url
  },
  DELETE_REDIRECT(state, id) {
    const redirects = state.redirects.filter((redirect) => {
      return redirect.id !== id
    })
    state.redirects = redirects
  }
}

export const actions = {
  async fetchRedirects({ commit }) {
    const { data } = await RedirectService.getAll()
    commit('SET_REDIRECTS', data)
  },
  async createRedirect({ commit }, redirect) {
    try {
      const { data } = await RedirectService.create(redirect)
      commit('ADD_REDIRECT', data)
      Notification.open({
        message: 'New redirect added.',
        hasIcon: true,
        type: 'is-success',
        position: 'is-bottom-right'
      })
    } catch (e) {
      Notification.open({
        message: 'Error creating redirect.',
        hasIcon: true,
        type: 'is-danger',
        position: 'is-bottom-right'
      })
      throw e
    }
  },
  async editRedirect({ commit }, updatedRedirect) {
    try {
      await RedirectService.update(updatedRedirect)
      commit('EDIT_REDIRECT', updatedRedirect)
      Notification.open({
        message: 'Redirect successfully updated.',
        hasIcon: true,
        type: 'is-success',
        position: 'is-bottom-right'
      })
    } catch (e) {
      Notification.open({
        message: 'Error updating redirect.',
        hasIcon: true,
        type: 'is-danger',
        position: 'is-bottom-right'
      })
      throw e
    }
  },
  async deleteRedirect({ commit }, id) {
    try {
      await RedirectService.delete(id)
      commit('DELETE_REDIRECT', id)
      Notification.open({
        message: 'Redirect deleted.',
        hasIcon: true,
        type: 'is-warning',
        position: 'is-bottom-right'
      })
    } catch (e) {
      Notification.open({
        message: 'Error deleting redirect.',
        hasIcon: true,
        type: 'is-danger',
        position: 'is-bottom-right'
      })
      throw e
    }
  }
}
